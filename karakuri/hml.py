"""
Implementation of Hennessy-Milner logic  over regular languages.

Formalism based on:

https://www.mcrl2.org/web/user_manual/articles/basic_modelling.html
"""
from typing import *
import typing
from dataclasses import dataclass

from .regular import NFA, nfa_to_dfa, dfa_to_nfa


A = TypeVar('A')

def _deserialize_bin(ser:Callable[[Any],A], fact:Callable[[A,A],A], data:Any) -> A:
    if isinstance(data, list):
        if len(data) == 2:
            return fact(ser(data[0]), ser(data[1]))
    if isinstance(data, dict):
        if len(data) == 2 and "left" in data and "right" in data:
            return fact(ser(data["left"]), ser(data["right"]))
    raise ValueError(data)

class Action(Generic[A]):
    def interpret(self, alphabet:Collection[A]) -> FrozenSet[A]:
        raise NotImplementedError()

    def serialize(self, all:Any=1, empty:Any=0) -> Any:
        if isinstance(self, AAll):
            return all
        if isinstance(self, AEmpty):
            return empty
        if isinstance(self, AAtom):
            return self.atom
        if isinstance(self, ANot):
            return {"not": self.action.serialize()}
        if isinstance(self, AUnion):
            return {"union": [self.left.serialize(), self.right.serialize()]}
        if isinstance(self, AIntersection):
            return {"intersect": [self.left.serialize(), self.right.serialize()]}
        if isinstance(self, AAtom):
            return self.atom

    @classmethod
    def deserialize(cls, data:Any, all:Any=1, empty:Any=0) -> "Action[A]":
        if data == all:
            return cast(Action[A], A_ALL)
        elif data == empty:
            return cast(Action[A], A_EMPTY)
        elif isinstance(data, dict):
            if "not" in data:
                return ANot(cls.deserialize(data["not"]))
            if "union" in data:
                return _deserialize_bin(cls.deserialize, AUnion, data["union"])
            if "intersect" in data:
                return _deserialize_bin(cls.deserialize, AIntersection, data["intersect"])
            raise ValueError(data)
        return AAtom(data)

@dataclass(frozen=True)
class AAll(Action[A]):
    def interpret(self, alphabet:Collection[A]) -> FrozenSet[A]:
        return frozenset(alphabet)
A_ALL:Action[Any] = AAll()

@dataclass(frozen=True)
class AEmpty(Action[A]):
    def interpret(self, alphabet:Collection[A]) -> FrozenSet[A]:
        return frozenset()
A_EMPTY:Action[Any] = AEmpty()

@dataclass(frozen=True)
class AAtom(Action[A]):
    atom:A
    def interpret(self, alphabet:Collection[A]) -> FrozenSet[A]:
        if self.atom not in alphabet:
            raise ValueError(f"{self.atom} not in {alphabet}")
        return frozenset([self.atom])

@dataclass(frozen=True)
class ANot(Action[A]):
    action:Action[A]
    def interpret(self, alphabet:Collection[A]) -> FrozenSet[A]:
        return frozenset(alphabet) - self.action.interpret(alphabet)

@dataclass(frozen=True)
class AUnion(Action[A]):
    left:Action[A]
    right:Action[A]
    def interpret(self, alphabet:Collection[A]) -> FrozenSet[A]:
        return self.left.interpret(alphabet).union(self.right.interpret(alphabet))

@dataclass(frozen=True)
class AIntersection(Action[A]):
    left:Action[A]
    right:Action[A]
    def interpret(self, alphabet:Collection[A]) -> FrozenSet[A]:
        return self.left.interpret(alphabet).intersection(self.right.interpret(alphabet))

################################################################################

Diag = typing.Union["FPossibly[A]", "FNecessarily[A]"]

def _serialize_nested(cls:"Type[Diag[A]]", elem:"Diag[A]") -> List[Any]:
    acts = []
    formula:Optional[Formula[A]] = None
    curr:Optional[Diag[A]] = elem
    del elem
    while curr is not None:
        acts.append(curr.action.serialize())
        if isinstance(curr.formula, cls):
            curr = cast("Diag[A]", curr.formula)
        else:
            formula = curr.formula
            curr = None
    assert formula is not None
    acts.append(formula.serialize())
    return acts

class Formula(Generic[A]):
    def interpret(self, alphabet:Collection[A]) -> NFA[Any,A]:
        raise NotImplementedError()

    def serialize(self) -> Any:
        if isinstance(self, FTrue):
            return True
        if isinstance(self, FFalse):
            return False
        if isinstance(self, FPossibly):
            acts = _serialize_nested(FPossibly, self)
            return dict(possibly=acts)
        if isinstance(self, FNecessarily):
            acts = _serialize_nested(FNecessarily, self)
            return dict(necessarily=acts)
        if isinstance(self, FNot):
            return {"not": self.formula.serialize()}
        if isinstance(self, FAnd):
            return {"and": [self.left.serialize(), self.right.serialize()]}
        if isinstance(self, FOr):
            return {"or": [self.left.serialize(), self.right.serialize()]}
        raise ValueError(self)

    @classmethod
    def _deserialize_nested(cls, fact:Callable[[Action[A],"Formula[A]"],"Formula[A]"], elems:Iterable[Any], all:Any, empty:Any) -> "Formula[A]":
            elems = list(elems)
            elems.reverse()
            result = fact(
                Action.deserialize(elems[1], all=all, empty=empty),
                cls.deserialize(elems[0], all=all, empty=empty),
            )
            for elem in elems[2:]:
                action:Action[A] = Action.deserialize(elem, all=all, empty=empty)
                result = fact(action, result)
            return result

    @classmethod
    def deserialize(cls, data:Any, all:Any=1, empty:Any=0) -> "Formula[A]":
        if data == True:
            return cast(Formula[A], F_TRUE)
        if data == False:
            return cast(Formula[A], F_FALSE)
        if isinstance(data, dict):
            if "possibly" in data and len(data) == 1 and isinstance(data["possibly"], list) and len(data["possibly"]) >= 2:
                return cls._deserialize_nested(FPossibly, data["possibly"], all, empty)
            if "necessarily" in data and len(data) == 1 and isinstance(data["necessarily"], list) and len(data["necessarily"]) >= 2:
                return cls._deserialize_nested(FNecessarily, data["necessarily"], all, empty)
            if "not" in data and len(data) == 1:
                return FNot(cls.deserialize(data["not"]))
            if "and" in data and len(data["and"]) == 2:
                return _deserialize_bin(cls.deserialize, FAnd, data["and"])
            if "or" in data and len(data["or"]) == 2:
                return _deserialize_bin(cls.deserialize, FOr, data["or"])
        raise ValueError(type(data), data)

@dataclass(frozen=True)
class FTrue(Formula[A]):
    def interpret(self, alphabet:Collection[A]) -> NFA[Any,A]:
        return NFA[Any,A].make_all(alphabet)

F_TRUE:Formula[Any] = FTrue()

@dataclass(frozen=True)
class FFalse(Formula[A]):
    def interpret(self, alphabet:Collection[A]) -> NFA[Any,A]:
        return NFA[Any,A].make_void(alphabet)

F_FALSE:Formula[Any] = FFalse()

@dataclass(frozen=True)
class FPossibly(Formula[A]):
    action: Action[A]
    formula: Formula[A]
    def interpret(self, alphabet:Collection[A]) -> NFA[Any,A]:
        chars = self.action.interpret(alphabet)
        prefix:NFA[Any,A] = NFA[Any,A].make_any_from(chars, alphabet)
        return prefix.concat(self.formula.interpret(alphabet))

@dataclass(frozen=True)
class FNecessarily(Formula[A]):
    action: Action[A]
    formula: Formula[A]
    def interpret(self, alphabet:Collection[A]) -> NFA[Any,A]:
        return FNot(FPossibly(self.action, FNot(self.formula))).interpret(alphabet)

@dataclass(frozen=True)
class FNot(Formula[A]):
    formula: Formula[A]
    def interpret(self, alphabet:Collection[A]) -> NFA[Any,A]:
        if isinstance(self.formula, FTrue):
            return F_FALSE.interpret(alphabet)
        if isinstance(self.formula, FFalse):
            return F_TRUE.interpret(alphabet)
        if isinstance(self.formula, FNot):
            return self.formula.formula.interpret(alphabet)
        if isinstance(self.formula, FAnd):
            left = FNot(self.formula.left)
            right = FNot(self.formula.right)
            return FOr(left, right).interpret(alphabet)
        if isinstance(self.formula, FOr):
            left = FNot(self.formula.left)
            right = FNot(self.formula.right)
            return FAnd(left, right).interpret(alphabet)
        if isinstance(self.formula, FNecessarily):
            action = self.formula.action
            formula = self.formula.formula
            return FPossibly(action, FNot(formula)).interpret(alphabet)
        # Simple complement via DFA
        nfa = self.formula.interpret(alphabet)
        dfa = nfa_to_dfa(nfa).complement()
        return dfa_to_nfa(dfa)

@dataclass(frozen=True)
class FAnd(Formula[A]):
    left: Formula[A]
    right: Formula[A]
    def interpret(self, alphabet:Collection[A]) -> NFA[Any,A]:
        left = nfa_to_dfa(self.left.interpret(alphabet))
        right = nfa_to_dfa(self.right.interpret(alphabet))
        return dfa_to_nfa(left.intersection(right))

@dataclass(frozen=True)
class FOr(Formula[A]):
    left: Formula[A]
    right: Formula[A]
    def interpret(self, alphabet:Collection[A]) -> NFA[Any,A]:
        left = self.left.interpret(alphabet)
        right = self.right.interpret(alphabet)
        return NFA.union(left, right)

__all__ = [
    "Action", "AAll", "AEmpty", "AAtom", "ANot", "AUnion", "AIntersection",
    "A_ALL", "A_EMPTY", "Formula", "FTrue", "FFalse", "F_TRUE", "F_FALSE",
    "FAnd", "FOr", "FNot", "FPossibly", "FNecessarily"
]
