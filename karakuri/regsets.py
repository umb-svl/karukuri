from .regular import *

from typing import (
    Generic,
    TypeVar,
    Optional,
    Any,
    Callable,
    Iterable,
    Collection,
)
A = TypeVar('A')

from dataclasses import dataclass, astuple, field

@dataclass
class RegularSet(Generic[A]):
    """
    Abstract away directly using a DFA or an NFA.
    """
    dfa: Optional[DFA[Any,A]]
    nfa: Optional[NFA[Any,A]]
    nfa_factory:Optional[Callable[[],NFA[Any,A]]] = field(default=None)
    dfa_factory:Optional[Callable[[],DFA[Any,A]]] = field(default=None)

    def __contains__(self, inputs:Iterable[A]) -> bool:
        if self.dfa is not None:
            return inputs in self.dfa
        if self.nfa is not None:
            return inputs in self.nfa
        return inputs in self.get_nfa()

    def get_dfa(self) -> DFA[Any,A]:
        if self.dfa is None:
            if self.dfa_factory is None:
                self.dfa = nfa_to_dfa(self.nfa)
            else:
                self.dfa = self.dfa_factory()
        return self.dfa

    def get_nfa(self) -> NFA[Any,A]:
        if self.nfa is None:
            if self.nfa_factory is None:
                self.nfa = dfa_to_nfa(self.dfa)
            else:
                self.nfa = self.nfa_factory()
        return self.nfa

    @classmethod
    def make_nfa_from_dict(cls, data):
        return RegularSet(
            nfa=NFA.from_dict(data),
            dfa=None,
        )

    @classmethod
    def make_dfa_from_dict(cls, data):
        return RegularSet(
            dfa=DFA.from_dict(data),
            nfa=None,
        )

    @classmethod
    def make_nil(cls, alphabet:Collection[A]) -> "Type[RegularSet[A]]":
        return RegularSet(
            nfa=NFA.make_nil(alphabet),
            dfa=NFA.make_nil(alphabet),
        )

    @classmethod
    def make_any(cls, alphabet:Collection[A]) -> "Type[RegularSet[A]]":
        return RegularSet(
            nfa=NFA.make_any(alphabet),
            dfa=DFA.make_any(alphabet),
        )

    @classmethod
    def make_void(cls, alphabet:Collection[A]) -> "Type[RegularSet[A]]":
        return RegularSet(
            nfa=NFA.make_void(alphabet),
            dfa=DFA.make_void(alphabet),
        )

    @classmethod
    def make_char(cls, alphabet:Collection[A], char:A) -> "Type[RegularSet[A]]":
        return RegularSet(
            nfa=NFA.make_char(alphabet, char),
            dfa=DFA.make_char(alphabet, char),
        )

    @classmethod
    def make_any_from(cls,  alphabet:Collection[A], chars:Collection[A]) -> "RegularSet[A]":
        return RegularSet(
            nfa=NFA.make_any_from(alphabet=alphabet, chars=chars),
            dfa=DFA.make_any_from(alphabet=alphabet, chars=chars),
        )

    @classmethod
    def make_all(cls,  alphabet:Collection[A]) -> "RegularSet[A]":
        return RegularSet(
            nfa=NFA.make_all(alphabet),
            dfa=DFA.make_all(alphabet),
        )

    def intersection(self, other:"RegularSet[A]") -> "RegularSet[A]":
        return RegularSet(
            dfa=self.get_dfa().intersection(other.get_dfa()),
            nfa=None,
        )

    def union(self, other:"RegularSet[A]") -> "RegularSet[A]":
        if self.dfa is not None and other.dfa is not None:
            dfa = self.dfa.union(other.dfa)
        else:
            dfa = None

        if self.nfa is not None and other.nfa is not None:
            nfa = self.nfa.union(other.nfa)
        else:
            nfa = None

        return RegularSet(
            dfa=dfa,
            nfa=nfa,
            nfa_factory=lambda: self.get_nfa().union(other.get_nfa()),
            dfa_factory=lambda: self.get_dfa().union(other.get_dfa()),
        )

    def remove_sink_states(self):
        return RegularSet(
            nfa = self.get_nfa().remove_sink_states(),
            dfa = None,
        )

    def remove_epsilon_transitions(self):
        if self.dfa is not None:
            return self

        return RegularSet(
            nfa = self.get_nfa().remove_epsilon_transitions(),
            dfa = None,
        )

    def filter_char(self, regexp):
        pattern = re.compile(regexp)
        def on_elem(x):
            return x if x is None else pattern.match(x)
        return RegularSet(
            nfa=self.get_nfa().filter_char(on_elem),
            dfa=None,
        )

    def product(self, other:"RegularSet[A]") -> "RegularSet[A]":
        return RegularSet(
            dfa=self.get_dfa().product(other.get_dfa()),
            nfa=None,
        )

    def subtract(self, other:"RegularSet[A]") -> "RegularSet[A]":
        return RegularSet(
            dfa=self.get_dfa().subtract(other.get_dfa()),
            nfa=None,
        )

    def complement(self) -> "RegularSet[A]":
        return RegularSet(
            dfa=self.get_dfa().complement(),
            nfa=None,
        )

    def shuffle(self, other:"RegularSet[A]") -> "RegularSet[A]":
        return RegularSet(
            nfa=self.get_nfa().shuffle(other.get_nfa()),
            dfa=None,
        )

    def concat(self, other:"RegularSet[A]") -> "RegularSet[A]":
        return RegularSet(
            nfa=self.get_nfa().concat(other.get_nfa()),
            dfa=None,
        )

    def minimize(self):
        return RegularSet(
            dfa = self.get_dfa().minimize(),
            nfa = None,
        )

    def star(self) -> "RegularSet[A]":
        return RegularSet(
            nfa=self.get_nfa().star(),
            dfa=None,
        )

    # Boolean
    def contains(self, other:"RegularSet[A]") -> bool:
        return self.get_dfa().contains(other.get_dfa())

    def is_equivalent_to(self, other:"RegularSet[A]") -> bool:
        return self.get_dfa().is_equivalent_to(self.other.get_dfa())

@dataclass
class SymbolicSet(Generic[A]):
    def reduce(self, alphabet:Collection[A]):
        raise NotImplementedError()

@dataclass
class Nil(SymbolicSet[A]):
    def reduce(self, alphabet):
        return RegularSet.make_nil(alphabet)

@dataclass
class Void(SymbolicSet[A]):
    def reduce(self, alphabet):
        return RegularSet.make_void(alphabet)

@dataclass
class Any(SymbolicSet[A]):
    def reduce(self, alphabet):
        return RegularSet.make_any(alphabet)

@dataclass
class All(SymbolicSet[A]):
    def reduce(self, alphabet):
        return RegularSet.make_all(alphabet)

@dataclass
class Void(SymbolicSet[A]):
    def reduce(self, alphabet):
        return RegularSet.make_void(alphabet)

@dataclass
class One(SymbolicSet[A]):
    char:A
    def reduce(self, alphabet):
        return RegularSet.make_char(alphabet=alphabet, char=self.char)


@dataclass
class Choice(SymbolicSet[A]):
    options: Collection[A]
    def reduce(self, alphabet):
        return RegularSet.make_any_from(
            alphabet=alphabet,
            chars=self.options
        )

@dataclass
class Binop(SymbolicSet[A]):
    left: SymbolicSet[A]
    right: SymbolicSet[A]

@dataclass
class Unop(SymbolicSet[A]):
    element: SymbolicSet[A]

def bin_reduce(f):
    def reduce(self, alphabet):
        left = self.left.reduce(alphabet)
        right = self.right.reduce(alphabet)
        return f(left, right)
    return reduce


class Intersection(Binop[A]):
    reduce = bin_reduce(RegularSet.intersection)

class Union(Binop[A]):
    reduce = bin_reduce(RegularSet.union)

class Product(Binop[A]):
    reduce = bin_reduce(RegularSet.product)

class Subtract(Binop[A]):
    reduce = bin_reduce(RegularSet.subtract)

class Shuffle(Binop[A]):
    reduce = bin_reduce(RegularSet.shuffle)

class Concat(Binop[A]):
    reduce = bin_reduce(RegularSet.concat)

class Star(Unop[A]):
    def reduce(self, alphabet):
        return self.element.reduce(alphabet).star()

class Complement(Unop[A]):
    def reduce(self, alphabet):
        return self.element.reduce(alphabet).complement()
