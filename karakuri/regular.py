from itertools import combinations, chain, product
import itertools
from typing import (Generic, TypeVar, Set, Any, Iterator, Tuple, List,
    Optional, Collection, Mapping, Dict, Callable, Type, AbstractSet,
    Iterable, FrozenSet, Sequence, NamedTuple, cast
)
import typing
from dataclasses import dataclass, astuple, field
import enum

__all__ = (
    "DFA",
    "NFA",
    "nfa_to_dfa",
    "dfa_to_nfa",
    "Regex",
    "Star",
    "Union",
    "Concat",
    "Nil",
    "Void",
    "Char",
    "regex_to_nfa",
    "nfa_to_regex",
    "VOID",
    "NIL",
)

S = TypeVar('S')
S1 = TypeVar('S1')
S2 = TypeVar('S2')
A = TypeVar('A')
A1 = TypeVar('A1')
B = TypeVar('B')

StateDiagram = Tuple[Collection[S],Mapping[Tuple[S,S], Collection[A]]]

def wrap_accepted_states(accepted_states:typing.Union[ Callable[[S], bool], Collection[S] ]) -> Callable[[S], bool]:
    if callable(accepted_states):
        return accepted_states
    elems:Collection[S] = accepted_states
    return lambda x: x in elems


def tag(st:S1, iterable:Iterable[S2]) -> Iterable[Tuple[S1,S2]]:
    return map(lambda x: (st, x), iterable)

class TransitionLookup:
    grouped_transitions:List[Dict[int,int]]
    cache: Dict[Tuple[int,int],Set[Tuple[int,int]]]

    def __init__(self, alphabet:Collection[A], transitions:Dict[Tuple[int,A],int]):
        l_alphabet = list(alphabet)
        alpha_to_idx = dict((k,idx) for idx, k in enumerate(l_alphabet))
        self.grouped_transitions = [dict() for _ in range(len(l_alphabet))]
        for ((src, char), dst) in transitions.items():
            idx = alpha_to_idx[char]
            self.grouped_transitions[idx][src] = dst
        self.cache = dict()

    def get_edges(self, edge:Tuple[int,int]) -> Iterator[Tuple[int,int]]:
        src, dst = edge
        # for some a in alphabet
        for tsx in self.grouped_transitions:
            # {tsx(p,a),tsx(q,a)} is marked
            # new_src, new_dst = {tsx(p,a),tsx(q,a)}
            # We sort the tuple because of symmetry
            next_p:int = tsx[src]
            next_q:int = tsx[dst]
            # ignore transitions to same
            if next_p != next_q:
                yield(min(next_p,next_q),max(next_p,next_q))

    def get_next_transitions(self, edge:Tuple[int,int]) -> Set[Tuple[int,int]]:
        result = self.cache.get(edge)
        if result is None:
            result = set(self.get_edges(edge))
            self.cache[edge] = result
        return result

    def has_unmarked_edge(self, edge:Tuple[int,int], edges:Set[Tuple[int,int]]) -> bool:
        tsx = self.get_next_transitions(edge)
        for next_edge in tsx:
            if next_edge not in edges:
                # This edge is no longer needed, so we can clear its cache
                del self.cache[edge]
                return True
        return False

    def get_next_to_mark(self, edges:Set[Tuple[int,int]]) -> Optional[Tuple[int,int]]:
        for edge in edges:
            if self.has_unmarked_edge(edge, edges):
                return edge
        return None

def minimize_transitions(alphabet:Collection[A], state_count:int, accepted_states:Set[int], transitions:Dict[Tuple[int,A],int]) -> Dict[Tuple[int,A],int]:
    tsx = TransitionLookup(alphabet, transitions)
    # https://www.cs.cornell.edu/courses/cs2800/2013fa/Handouts/minimization.pdf
    # marked as soon as a reason is discovered why p and q are not equivalent
    # Note: the table is symmetric, so we only store smaller-larger pairs
    unmarked = set()
    edges = set(
        (src, dst)
        for src in range(state_count)
            for dst in range(src + 1, state_count)
    )
    # Step 2: Mark{p,q} if 'p in F' XOR 'q in F'
    def is_unmarked(pq:Tuple[int,int]) -> bool:
        p, q = pq
        return (p in accepted_states) == (q in accepted_states)
    # Unmarked entries are the ones where p in F <-> q in F
    # Aka, Mark {p,q} == false
    unmarked = set(filter(is_unmarked, edges))
    # Step 3. Repeat the following until no more changes occur: if there exists
    # a pair{p,q} in next_to_mark then mark{p,q}.
    running = True
    while running:
        running = False
        pair = tsx.get_next_to_mark(unmarked)
        if pair is not None:
            unmarked.remove(pair)
            running = True
    # Finally we are ready to create the new automaton
    # p = q iff {p,q} unmarked
    # Unmarked means equivalent, if there are no equivalence classes
    # Then, there is nothing to minimize
    if len(unmarked) == 0:
        return transitions
    # Now we know which states are different
    # We build an array, such that for each state we know its
    # equivalent ancestor (self-loop means no other is same)
    states = list(range(state_count))
    for pq in edges:
        pq_same = pq in unmarked
        if pq_same:
            small, big = sorted(pq)
            states[big] = small
    # Given a state, returns the unique ancestor of each state
    def get_state(st:int) -> int:
        # Jump through list to get ancestor
        # Keep around the original state, so that we can backtrack and update
        # The root ancestor for all
        old_st = st
        while states[st] != st:
            st = states[st]
        # Cache the new root ancestor
        parent_st = states[old_st]
        while parent_st != old_st:
            if parent_st == st:
                break
            states[old_st] = st
            old_st = parent_st
            parent_st = states[old_st]
        return st

    new_transitions = {}
    for (src,char), dst in transitions.items():
        # For each transition, only store the canonical state
        src = get_state(src)
        dst = get_state(dst)
        new_transitions[(src,char)] = dst
    return new_transitions

DTransitionFunc = Callable[[S,A], S]

class DFA(Generic[S,A]):
    def __init__(self, alphabet:Collection[A],
                    transition_func:DTransitionFunc[S,A],
                    start_state:S,
                    accepted_states:typing.Union[Collection[S],Callable[[S], bool]]):
        self.alphabet = alphabet
        self.transition_func = transition_func
        self.start_state = start_state
        self.accepted_states = wrap_accepted_states(accepted_states)

    def __contains__(self, inputs:Iterable[A]) -> bool:
        return self.accepts(inputs)

    def accepts(self, inputs:Iterable[A]) -> bool:
        st = self.start_state
        for i in inputs:
            #assert i in self.alphabet, f"{i} not in {self.alphabet}"
            st = self.transition_func(st, i)
            #assert st in self.states, f"{st} not in {self.states}"
        return self.accepted_states(st)

    def is_sink(self, st:S) -> bool:
        """
        Tests if a state is a sink node (only contains self loops)
        """
        if self.accepted_states(st):
            return False
        out = set(st for char, st in self.get_outgoing(st))
        return len(out) == 1 and st in out

    def get_divergence_index(self, chars:Iterable[A]) -> Optional[int]:
        """
        If a string is rejected, we may want to know what is the shortest
        substring that is accepted by the automaton. The algorithm expects
        a *minimized* automaton.

        Suppose the automaton only accepts '0*' and we want to know the
        divergence index of string '01', the divergence index should be 1,
        because it is the first index that has no further possibilities,
        whichever string you extend it (eg, 010 fails, 011 fails, 0110 fails,
        and so on).

        Usage example:

        >>> a = DFA(alphabet=[0, 1],
        ...     transition_func=DFA.transition_table({
        ...         ("q_1", 0): "q_1",
        ...         ("q_1", 1): "q_2",
        ...         ("q_2", 0): "q_2",
        ...         ("q_2", 1): "q_2",
        ...     }),
        ...     start_state="q_1",
        ...     accepted_states=["q_1"],
        ... )
        >>> a.get_divergence_index([])
        >>> a.get_divergence_index([1])
        0
        >>> a.get_divergence_index([0,1])
        1
        >>> a.get_divergence_index([0,0,1])
        2
        >>> a.get_divergence_index([0,0,0,1])
        3
        """
        idx = 0
        st = self.start_state
        if self.is_sink(st):
            return None if self.accepted_states(st) else idx
        for i in chars:
            st = self.transition_func(st, i)
            if self.is_sink(st):
                return None if self.accepted_states(st) else idx
            idx += 1
        return None if self.accepted_states(st) else idx - 1


    def product(dfa1:"DFA[S1,A]", dfa2:"DFA[S2,A]", accepted_states:typing.Union[Collection[Tuple[S1,S2]],Callable[[Tuple[S1,S2]], bool]]) -> "DFA[Tuple[S1,S2], A]":
        assert frozenset(dfa1.alphabet) == frozenset(dfa2.alphabet)
        tsx1 = dfa1.transition_func
        tsx2 = dfa2.transition_func
        def transition(q:Tuple[S1,S2], a:A) -> Tuple[S1,S2]:
            q1, q2 = q
            return (tsx1(q1, a), tsx2(q2, a))

        return DFA(alphabet=dfa1.alphabet,
                   transition_func=transition,
                   start_state=(dfa1.start_state, dfa2.start_state),
                   accepted_states=accepted_states)

    def union(dfa1:"DFA[S1,A]", dfa2:"DFA[S2,A]") -> "DFA[Tuple[S1,S2], A]":
        acc1:Callable[[S1], bool] = dfa1.accepted_states
        acc2:Callable[[S2], bool] = dfa2.accepted_states
        def is_final(q:Tuple[S1,S2]) -> bool:
            q1, q2 = q
            return acc1(q1) or acc2(q2)
        return dfa1.product(dfa2, is_final)

    def intersection(dfa1:"DFA[S1,A]", dfa2:"DFA[S2,A]") -> "DFA[Tuple[S1,S2], A]":
        acc1:Callable[[S1], bool] = dfa1.accepted_states
        acc2:Callable[[S2], bool] = dfa2.accepted_states
        def is_final(q:Tuple[S1,S2]) -> bool:
            q1, q2 = q
            return acc1(q1) and acc2(q2)
        return dfa1.product(dfa2, is_final)

    def subtract(dfa1:"DFA[S1,A]", dfa2:"DFA[S2,A]") -> "DFA[Tuple[S1,S2], A]":
        # We use a direct encoding of subtraction to optimize performance
        acc1:Callable[[S1], bool] = dfa1.accepted_states
        acc2:Callable[[S2], bool] = dfa2.accepted_states
        def is_final(q:Tuple[S1,S2]) -> bool:
            q1, q2 = q
            return acc1(q1) and not acc2(q2)
        return dfa1.product(dfa2, is_final)

    def complement(dfa:"DFA[S,A]") -> "DFA[S,A]":
        is_final = dfa.accepted_states
        return DFA(
            alphabet = dfa.alphabet,
            transition_func = dfa.transition_func,
            start_state = dfa.start_state,
            accepted_states = lambda x: not is_final(x)
        )

    def contains(dfa1:"DFA[S1,A]", dfa2:"DFA[S2,A]") -> bool:
        return dfa2.subtract(dfa1).is_empty()

    def set_alphabet(dfa:"DFA[S,A]", alphabet:Collection[A1]) -> "DFA[Tuple[int,typing.Union[S,int]],typing.Union[A,A1]]":
        """
        Creates a new DFA that uses the same transition function, but a new
        alphabet. Essentially, this operation performs intersects the dfa
        alphabet with the given alphabet.
        """
        tsx = dfa.transition_func
        alpha = frozenset(dfa.alphabet).intersection(alphabet)
        def transition_func(pair:Tuple[int,typing.Union[S,int]], a:typing.Union[A,A1]) -> Tuple[int,typing.Union[S,int]]:
            idx, q = pair
            if idx == 0 or a not in alpha:
                return (0, 0)
            return (1, tsx(cast(S,q), cast(A,a)))
        acc = dfa.accepted_states
        def is_final(pair:Tuple[int,typing.Union[S,int]]) -> bool:
            idx, q = pair
            return idx == 1 and acc(cast(S,q))

        return DFA(
            alphabet = alphabet,
            transition_func = transition_func,
            start_state = (1, dfa.start_state),
            accepted_states = is_final,
        )


    @property
    def edges(dfa) -> Iterator[Tuple[S, A, S]]:
        visited_nodes : Set[S] = set()
        to_visit = [dfa.start_state]
        while len(to_visit) > 0:
            src = to_visit.pop()
            if src in visited_nodes:
                continue
            # New node
            visited_nodes.add(src)
            for char, dst in dfa.get_outgoing(src):
                yield (src, char, dst)
                if dst not in visited_nodes:
                    to_visit.append(dst)

    def iter_outgoing(dfa) -> Iterator[Tuple[S,List[Tuple[A,S]]]]:
        last_src:Optional[S] = None
        outs:List[Tuple[A,S]] = []
        for (src,char,dst) in dfa.edges:
            assert src is not None
            # only on the first iteration
            if last_src is None:
                last_src = src
            # all iterations
            if src is not last_src:
                yield last_src, outs
                last_src = src
                outs = [(char, dst)]
            else:
                outs.append((char, dst))
        if last_src is not None:
            yield last_src, outs


    def get_outgoing(self, st:S) -> Iterator[Tuple[A,S]]:
        for char in self.alphabet:
            yield (char, self.transition_func(st, char))


    def flatten(self:"DFA[S,A]", minimize:bool=False) -> "DFA[int, A]":
        """
        Flatten returns a new DFA that caches all of its transitions,
        essentially improving time but reducing space.
        """
        state_to_id:Dict[S,int] = {}
        def get_state_id(st:S) -> int:
            st_id = state_to_id.get(st, None)
            if st_id is None:
                st_id = len(state_to_id)
                state_to_id[st] = st_id
            return st_id

        transitions = {}
        accepted_states:List[int] = []
        for (src, out) in self.iter_outgoing():
            src_id = get_state_id(src)
            if self.accepted_states(src):
                accepted_states.append(src_id)
            for (char, dst) in out:
                dst_id = get_state_id(dst)
                transitions[(src_id, char)] = dst_id
        if minimize:
            transitions = minimize_transitions(
                self.alphabet,
                len(state_to_id),
                set(accepted_states),
                transitions
            )
        return DFA(
            alphabet=self.alphabet,
            transition_func=DFA.transition_table(transitions),
            start_state=state_to_id[self.start_state],
            accepted_states=accepted_states.__contains__
        )

    def minimize(self) -> "DFA[int, A]":
        return self.flatten(minimize=True)

    @property
    def states(self) -> Iterator[S]:
        last_src = None
        for src, _, _ in self.edges:
            if src is not last_src:
                yield src
                last_src = src

    def is_empty(self) -> bool:
        for st in self.end_states:
            return False
        return True

    def is_equivalent_to(dfa1:"DFA[S1,A]", dfa2:"DFA[S2,A]") -> bool:
        return dfa1.contains(dfa2) and dfa2.contains(dfa1)

    def get_minimized_graph(dfa) -> StateDiagram[S,A]:
        edges:Dict[Tuple[S,S], Set[A]] = {}
        nodes:Set[S] = set()
        for src, out in dfa.iter_outgoing():
            nodes.add(src)
            for (char, dst) in out:
                src_dst = (src, dst)
                chars = edges.get(src_dst, None)
                if chars is None:
                    edges[src_dst] = chars = set()
                chars.add(char)
        return nodes, edges

    def get_full_graph(dfa) -> StateDiagram[S,A]:
        edges:Dict[Tuple[S,S],List[A]] = dict()
        for src in dfa.states:
            for char in dfa.alphabet:
                dst = dfa.transition_func(src, char)
                kv = (src,dst)
                chars = edges.get(kv, None)
                if chars is None:
                    edges[kv] = chars = []
                chars.append(char)
        return list(dfa.states), edges

    def find_shortest_path(d:"DFA[S,A]", find_state:Callable[[S],bool]) -> Optional[Tuple[A,...]]:
        """
        Returns the shortest path according to some state condition.
        """
        visited = set([d.start_state])
        if find_state(d.start_state):
            return ()
        to_process:List[Tuple[Tuple[A,...],S]]  = [((), d.start_state)]
        # Perform a breadth-first visit, while ensuring we don't visit
        # The same node more than once
        while len(to_process) > 0:
            next_frontier = []
            for (seq, src) in to_process:
                for c in d.alphabet:
                    next_st = d.transition_func(src, c)
                    if next_st in visited:
                        continue
                    next_seq = seq + (c,)
                    if find_state(next_st):
                        # Found the shortest string
                        return next_seq
                    else:
                        visited.add(next_st)
                        next_frontier.append((next_seq, next_st))
            to_process.clear()
            to_process = next_frontier
        return None

    def get_derivation(d:"DFA[S,A]", word:Iterable[A]) -> Iterable[S]:
        """
        Returns the derivation path for a given word.
        For each letter of the word, yields the next state.
        Does not yield the initial state.
        """
        st = d.start_state
        for c in word:
            next_st = d.transition_func(st, c)
            yield next_st
            st = next_st

    def get_shortest_string(d:"DFA[S,A]") -> Optional[Tuple[A,...]]:
        """
        Returns a string with the shortest length recognizes by this automaton.
        """
        return d.find_shortest_path(d.accepted_states)

    def as_dict(self:"DFA[S,A]", flatten:bool=True) -> Any:
        """
        Converts this DFA into a dictionary.
        The alphabet must be serializable.
        """
        if flatten:
            dfa:DFA[Any,A] = self.flatten()
        else:
            dfa = self
        del self
        edges = list(
            dict(src=src, dst=dst, char=char)
            for ((src,dst),chars) in dfa.as_graph()[1].items()
                for char in chars
        )
        return dict(
            start_state = dfa.start_state,
            accepted_states = list(dfa.end_states),
            edges = edges
        )

    @classmethod
    def from_dict(cls, dfa:Any, alphabet:Optional[Collection[A]]=None) -> "DFA[Any,A]":
        """
        Creates a DFA from a dictionary-based DFA (see `as_dict`).
        """
        if alphabet is None:
            alphabet = set(edge["char"] for edge in dfa["edges"])

        tbl = {}
        for edge in dfa["edges"]:
            src_char = (edge["src"], edge["char"])
            assert src_char not in tbl, f"duplicated edge: {src_char}"
            tbl[src_char] = edge["dst"]
        return cls(
            alphabet=alphabet,
            transition_func=DFA.transition_table(tbl),
            start_state=dfa["start_state"],
            accepted_states=set(dfa["accepted_states"]),
        )

    def __len__(self) -> int:
        """
        Returns how many states the DFA contains
        """
        count = 0
        for st in self.states:
            count += 1
        return count

    as_graph = get_minimized_graph

    def __eq__(self, other:Any) -> bool:
        if other is None:
            return False
        return isinstance(other, DFA) and \
                self.start_state == other.start_state and \
                self.as_graph() == other.as_graph() and \
                set(self.end_states) == set(other.end_states)

    @property
    def end_states(self) -> Iterator[S]:
        return filter(self.accepted_states, self.states)

    def __repr__(self) -> str:
        data = dict(start_state=self.start_state,
                    accepted_states=list(self.end_states),
                    transitions=dict(self.as_graph()[1]))
        return f'DFA({data})'

    @staticmethod
    def transition_table(table:Mapping[Tuple[S,A],S], sink:Optional[S]=None) -> DTransitionFunc[S,A]:
        if sink is None:
            def func(st:S, i:A) -> S:
                try:
                    return table[(st, i)]
                except KeyError as e:
                    raise ValueError("Invalid table", table) from e
        else:
            def func(st:S, i:A) -> S:
                assert sink is not None
                return table.get((st, i), sink)

        return func

    @classmethod
    def make_nil(cls:"Type[DFA[int,A]]", alphabet:Collection[A]) -> "DFA[int,A]":
        return cls(alphabet=alphabet,
                   transition_func=lambda q, a: 1,
                   start_state=0,
                   accepted_states=lambda x: x == 0)

    @classmethod
    def make_char(cls:"Type[DFA[int,A]]", alphabet:Collection[A], char:A) -> "DFA[int,A]":
        assert char in alphabet
        return cls(alphabet=alphabet,
                   transition_func=lambda q, a: 1
                   if q == 0 and a == char else 2,
                   start_state=0,
                   accepted_states=lambda x: x == 1)

    @classmethod
    def make_any(cls:"Type[DFA[int,A]]", alphabet:Collection[A]) -> "DFA[int, A]":
        Q1, Q2, Q3 = range(3)

        def transition(q:int, a:A) -> AbstractSet[int]:
            return Q2 if q == Q1 else Q3

        return cls(alphabet, transition, Q1, [Q2])

    @classmethod
    def make_any_from(cls:"Type[DFA[int,A]]", chars:Collection[A], alphabet:Collection[A]) -> "DFA[int, A]":
        if not frozenset(chars) <= frozenset(alphabet):
            raise ValueError(f"{chars} not a subset of {alphabet}")
        Q1, Q2, Q3 = range(3)

        def transition(q:int, a:A) -> AbstractSet[int]:
            return Q2 if a in chars and q == Q1 else Q3

        return cls(alphabet, transition, Q1, [Q2])

    @classmethod
    def make_void(cls:"Type[DFA[int,A]]", alphabet:Collection[A]) -> "DFA[int,A]":
        Q1 = 0

        def transition(q:int, a:A) -> int:
            return q

        return cls(alphabet=alphabet,
            transition_func=transition,
            start_state=Q1,
            accepted_states=(),
        )

    @classmethod
    def make_all(cls:"Type[DFA[int,A]]", alphabet:Collection[A]) -> "DFA[int, A]":
        def transition(q:int, a:A) -> int:
            return 0
        return cls(alphabet, transition, 0, [0])


def sample(alphabet:Collection[A], bound:Optional[int]=None) -> Iterator[Tuple[A,...]]:
    """
    Sample all words up to a certain bound.
    """
    count = 0
    while True:
        yield from product(alphabet, repeat=count)
        count += 1
        if bound is not None and count >= bound:
            break


################################################################################
# NFA

NTransitionFunc = Callable[[S,Optional[A]], AbstractSet[S]]
TransitionTable = Mapping[Tuple[S,Optional[A]],AbstractSet[S]]
NEdge = Tuple[S,Optional[A],S]

def multi_transition(transition_func:NTransitionFunc[S,A], states:Iterable[S], char:Optional[A]) -> FrozenSet[S]:
    new_states:Set[S] = set()
    for st in states:
        new_states.update(transition_func(st, char))
    return frozenset(new_states)

def epsilon(transition_func:NTransitionFunc[S,A], states:Iterable[S]) -> FrozenSet[S]:
    states = set(states)
    while True:
        count = len(states)
        states.update(multi_transition(transition_func, states, None))
        if count == len(states):
            return frozenset(states)

class StarState(Generic[S]):
    pass

@dataclass(frozen=True, order=True)
class StarState_Init(StarState[S]):
    pass

@dataclass(frozen=True, order=True)
class StarState_Other(StarState[S]):
    state:S

class ConcatState(Generic[S1,S2]):
    pass

@dataclass(frozen=True, order=True)
class ConcatState_Left(ConcatState[S1,S2]):
    state:S1

@dataclass(frozen=True, order=True)
class ConcatState_Right(ConcatState[S1,S2]):
    state:S2

@dataclass
class Derivation(Generic[S]):
    """
    Used in NFA.get_derivations to obtain how each transition was performed.
    """
    state:S
    parent:Optional["Derivation[S]"] = None

    def link(self, st:S):
        return Derivation(state=st, parent=self)

    def __iter__(self):
        if self.parent is not None:
            yield from self.parent
            yield self.state

    def __hash__(self):
        return hash(self.state)

    def __eq__(self, other:Any) -> bool:
        return (
            other is not None and
            isinstance(other, Derivation) and
            self.state == other.state
        )

def state_deserializer(st:Any) -> Any:
    if isinstance(st, list):
        return tuple(state_deserializer(x) for x in st)
    return st

def nfa_state_serializer(st:Any) -> Any:
    if isinstance(st, StarState_Init):
        return 0
    elif isinstance(st, StarState_Other):
        return [0, nfa_state_serializer(st.state)]
    elif isinstance(st, ConcatState_Left):
        return[0, nfa_state_serializer(st.state)]
    elif isinstance(st, ConcatState_Right):
        return [1, nfa_state_serializer(st.state)]
    if isinstance(st, tuple):
        return list(st)
    return st

class NFA(Generic[S,A]):

    def __init__(self, alphabet:Collection[A],
                    transition_func:NTransitionFunc[S,A],
                    start_state:S,
                    accepted_states:typing.Union[Collection[S],Callable[[S], bool]]):
        self.alphabet = alphabet
        self.transition_func = transition_func
        self.start_state = start_state
        self.accepted_states = wrap_accepted_states(accepted_states)

    def multi_transition(self, states:Iterable[S], input:Optional[A]) -> FrozenSet[S]:
        return multi_transition(self.transition_func, states, input)

    def epsilon(self, states:Iterable[S]) -> FrozenSet[S]:
        return epsilon(self.transition_func, states)

    def __contains__(self, inputs:Iterable[A]) -> bool:
        return self.accepts(inputs)

    def accepts(self, inputs:Iterable[A]) -> bool:
        # Perform epsilon transitions of the state
        states = self.epsilon({self.start_state})
        for i in inputs:
            if len(states) == 0:
                # Not accepted
                return False
            # Get the transitions and then perform epsilon transitions
            states = self.epsilon(self.multi_transition(states, i))
        return len(set(filter(self.accepted_states, states))) > 0

    def get_derivations(self, seq:Iterable[A]) -> Iterable[List[S]]:
        """
        Returns a sequence of all paths between initial and accepting states.
        The sequence is empty when the input is rejected. Two derivations are
        deemed equivalent if they share the same accepting state. The sequence
        returns only *distinct* derivations. An implication of this is that if
        there are two derivations that reach the same target state, the shortest
        is picked.
        """
        def tsx(st:Derivation[S], char:Optional[A]) -> FrozenSet[Derivation[S]]:
            return frozenset(map(st.link, self.transition_func(st.state, char)))
        states:FrozenSet[Derivation[S]] = epsilon(tsx, {Derivation(state=self.start_state)})
        for idx, i in enumerate(seq, 1):
            if len(states) == 0:
                # Not accepted
                return ()
            # Get the transitions and then perform epsilon transitions
            states = epsilon(tsx, multi_transition(tsx, states, i))
        return map(list, states)


    def union(nfa1:"NFA[S1,A]", nfa2:"NFA[S2,A]") -> "NFA[Tuple[int,typing.Union[S1,S2,int]], A]":
        tsx = (nfa1.transition_func, nfa2.transition_func)
        fin = (nfa1.accepted_states, nfa2.accepted_states)

        def transition(st:Tuple[int,typing.Union[S1,S2,int]], a:Optional[A]) -> AbstractSet[Tuple[int,typing.Union[S1,S2,int]]]:
            idx, st1 = st
            del st
            if idx == 2:
                return frozenset({(0, nfa1.start_state), (1, nfa2.start_state)
                                  }) if a is None else frozenset()

            return frozenset(tag(idx, tsx[idx](st1, a))) # type: ignore

        def is_final(st:Tuple[int,typing.Union[S1,S2,int]]) -> bool:
            idx, st1 = st
            if idx == 2:
                return False
            return fin[idx](st1) # type: ignore

        return NFA(
            alphabet=frozenset(nfa1.alphabet).union(nfa2.alphabet),
            transition_func=transition,
            accepted_states=is_final,
            start_state=(2, 0),
        )

    def shuffle(nfa1:"NFA[S1,A]", nfa2:"NFA[S2,A]") -> "NFA[Tuple[S1,S2],A]":
        def transition(st:Tuple[S1,S2], a:Optional[A]) -> AbstractSet[Tuple[S1,S2]]:
            (st1, st2) = st
            result:Set[Tuple[S1,S2]] = set()
            if a is None:
                result.update(
                    (st1, st2) for st1 in nfa1.transition_func(st1, a))
                result.update(
                    (st1, st2) for st2 in nfa2.transition_func(st2, a))
                return result

            if a in nfa1.alphabet:
                result.update(
                    (st1, st2) for st1 in nfa1.transition_func(st1, a))

            if a in nfa2.alphabet:
                result.update(
                    (st1, st2) for st2 in nfa2.transition_func(st2, a))

            return frozenset(result)

        def is_final(st:Tuple[S1,S2]) -> bool:
            st1, st2 = st
            return nfa1.accepted_states(st1) or nfa2.accepted_states(st2)

        return NFA(
            alphabet=frozenset(nfa1.alphabet).union(nfa2.alphabet),
            start_state=(nfa1.start_state, nfa2.start_state),
            accepted_states=is_final,
            transition_func=transition,
        )

    def map_alphabet(nfa:"NFA[S,A]", replace:Mapping[Optional[A], Optional[B]]) -> "NFA[S, B]":
        """Replaces every character of the source DFA using the given map.
        The map is a reverse entry, so key character in the target NFA
        replaces mapped value character in the source NFA. Multiple characters
        of the old NFA can point to the same key of the new NFA, which is
        why the map ranges to a collection of characters, rather than just one
        character."""
        new_to_old = dict()
        for (old_a, new_a) in replace.items():
            targets = new_to_old.get(new_a, None)
            if targets is None:
                targets = []
                new_to_old[new_a] = targets
            targets.append(old_a)
        old_tsx_func = nfa.transition_func
        new_alpha = frozenset(k for k in new_to_old if k is not None)

        def transition_func(src:S, char:Optional[A]) -> AbstractSet[S]:
            next_states = set()
            for old_char in new_to_old[char]:
                next_states.update(old_tsx_func(src, old_char))
            return next_states

        return NFA(
            alphabet=new_alpha,
            start_state=nfa.start_state,
            accepted_states=nfa.accepted_states,
            transition_func=transition_func,
        )

    def filter_char(nfa:"NFA[S,A]", to_keep:Callable[[Optional[A]], bool], update_alphabet:bool=True) -> "NFA[S,A]":
        """
        Filter out certain characters of the alphabet.
        """
        old_tsx_func = nfa.transition_func
        new_alpha = set(filter(to_keep, nfa.alphabet))
        to_remove = set(nfa.alphabet) - new_alpha

        def transition_func(src:S, char:Optional[A]) -> AbstractSet[S]:
            if char is not None:
                return old_tsx_func(src, char)
            # Otherwise:
            result = set(old_tsx_func(src, None))
            for elem in to_remove:
                result.update(old_tsx_func(src, elem))
            return result

        return NFA(
            alphabet=new_alpha if update_alphabet else nfa.alphabet,
            transition_func=transition_func,
            start_state=nfa.start_state,
            accepted_states=nfa.accepted_states,
        )



    def concat(nfa1:"NFA[S1,A]", nfa2:"NFA[S2,A]") -> "NFA[ConcatState[S1,S2],A]":
        s2:ConcatState[S1,S2] = ConcatState_Right(nfa2.start_state)
        start_right : FrozenSet[ConcatState[S1,S2]] = frozenset({s2})
        def transition(st:ConcatState[S1,S2], a:Optional[A]) -> AbstractSet[ConcatState[S1,S2]]:
            # Add an extra edge
            if isinstance(st, ConcatState_Left):
                s1 = st.state
                result = set(map(ConcatState_Left[S1,S2], nfa1.transition_func(s1, a)))
                if nfa1.accepted_states(s1) and a is None:
                    return start_right.union(result)
                else:
                    return result
            elif isinstance(st, ConcatState_Right):
                s2 = st.state
                return set(map(ConcatState_Right[S1,S2], nfa2.transition_func(s2, a)))
            raise ValueError()

        def is_final(st: ConcatState[S1,S2]) -> bool:
            return isinstance(st, ConcatState_Right) and nfa2.accepted_states(st.state)

        return NFA(
            alphabet=frozenset(nfa1.alphabet).union(nfa2.alphabet),
            transition_func=transition,
            accepted_states=is_final,
            start_state=ConcatState_Left[S1,S2](nfa1.start_state),
        )

    def star(nfa) -> "NFA[StarState[S],A]":
        INIT:StarState[S] = StarState_Init[S]()
        START:StarState[S] = StarState_Other(nfa.start_state)
        Q1 = frozenset([START])
        EMPTY:FrozenSet[StarState[S]] = frozenset()
        def transition(st:StarState[S], a:Optional[A]) -> AbstractSet[StarState[S]]:
            if isinstance(st, StarState_Init):
                return Q1 if a is None else EMPTY
            assert isinstance(st, StarState_Other)
            result:Set[StarState[S]] = set(map(StarState_Other[S], nfa.transition_func(st.state, a)))
            if a is None and nfa.accepted_states(st.state):
                result.add(INIT)  # Return back to the initial state
            return frozenset(result)

        return NFA(
            alphabet=nfa.alphabet,
            transition_func=transition,
            accepted_states=[INIT],
            start_state=INIT,
        )

    def flatten(self) -> "NFA[int, A]":
        """
        Flatten returns a new NFA that caches all of its transitions,
        essentially improving time but reducing space.
        """
        state_to_id:Dict[S,int] = {}
        def get_state_id(st:S) -> int:
            st_id = state_to_id.get(st, None)
            if st_id is None:
                st_id = len(state_to_id)
                state_to_id[st] = st_id
            return st_id

        transitions = {}
        accepted_states:List[int] = []
        for (src, out) in self.iter_outgoing():
            src_id = get_state_id(src)
            if self.accepted_states(src):
                accepted_states.append(src_id)
            for (char, dsts) in out:
                transitions[(src_id, char)] = frozenset(map(get_state_id, dsts))
        return NFA(
            alphabet=self.alphabet,
            transition_func=NFA.transition_table(transitions),
            start_state=state_to_id[self.start_state],
            accepted_states=set(accepted_states)
        )


    def get_edges(self) -> Mapping[Tuple[S,S], Collection[Optional[A]]]:
        return self.as_graph()[1]

    def get_outgoing(self, node:S) -> Iterator[Tuple[Optional[A],AbstractSet[S]]]:
        for char in self.alphabet:
            yield (char, self.transition_func(node, char))
        yield (None, self.transition_func(node, None))

    def get_incoming(self, node:S) -> Iterator[S]:
        return (src for (src, _, dst) in self.edges if dst == node)

    def __eq__(self, other:Any) -> bool:
        if other is None or not isinstance(other, NFA):
            return False
        # Create a homo-morphism on states
        n1 = self.flatten()
        n2 = other.flatten()
        return n1.start_state == n2.start_state and \
                n1.as_graph() == n2.as_graph() and \
                set(n1.end_states) == set(n2.end_states)


    def visit(self, st) -> Iterator[Tuple[S,Optional[A],AbstractSet[S]]]:
        visited_nodes : Set[S] = set()
        to_visit = [st]
        while len(to_visit) > 0:
            src = to_visit.pop()
            if src in visited_nodes:
                continue
            # New node
            visited_nodes.add(src)
            for char, dsts in self.get_outgoing(src):
                yield (src, char, dsts)
                for dst in dsts:
                    if dst not in visited_nodes:
                        to_visit.append(dst)

    @property
    def edges(nfa) -> Iterator[Tuple[S,Optional[A],AbstractSet[S]]]:
        return nfa.visit(nfa.start_state)

    def iter_outgoing(nfa) -> Iterator[Tuple[S,Collection[Tuple[Optional[A],AbstractSet[S]]]]]:
        last_src:Optional[S] = None
        outs:List[Tuple[Optional[A],AbstractSet[S]]] = []
        for (src,char,dsts) in nfa.edges:
            # only on the first iteration
            if last_src is None:
                last_src = src
            assert last_src is not None
            # all iterations
            if src is not last_src:
                yield last_src, outs
                last_src = src
                outs = [(char, dsts)]
            else:
                outs.append((char, dsts))
        if last_src is not None:
            yield last_src, outs

    def get_reachable(self, st:S) -> Iterator[S]:
        """
        Returns all reachable states.
        """
        last_src = None
        for src, _, _ in self.visit(st):
            if src is not last_src:
                yield src
                last_src = src

    @property
    def states(self) -> Iterator[S]:
        """
        Returns all reachable states.
        """
        return self.get_reachable(self.start_state)

    def as_graph(nfa) -> StateDiagram[S,Optional[A]]:
        visited_nodes:Set[S] = set()
        edges:Dict[Tuple[S,S],Set[Optional[A]]] = {}
        for (src, out) in nfa.iter_outgoing():
            visited_nodes.add(src)
            for (char, dsts) in out:
                for dst in dsts:
                    src_dst = (src,dst)
                    chars = edges.get(src_dst, None)
                    if chars is None:
                        edges[src_dst] = chars = set()
                    chars.add(char)
        return visited_nodes, edges

    def remove_epsilon_transitions(self) -> "NFA[S,A]":
        """
        Returns an equivalent NFA that does not have epsilon-transitions
        """
        def transition_func(src:S, char:Optional[A]) -> AbstractSet[S]:
            if char == None:
                return frozenset()
            # There are outgoing edges with epsilon transitions. Calculate the
            # epsilon-closure and perform a transition on each state
            return self.multi_transition(self.epsilon([src]), char)
        def accepted_states(st:S) -> bool:
            if self.accepted_states(st):
                return True
            for k in self.epsilon([st]):
                if self.accepted_states(k):
                    return True
            return False
        return NFA(self.alphabet, transition_func, self.start_state, accepted_states)


    def _get_reachable_sinks(self, st:S) -> Optional[List[S]]:
        """
        Returns the list of reachable sink nodes, which always includes itself.
        When the node is not sink return None.
        """
        if self.accepted_states(st):
            return None
        sinks:List[S] = []
        for dst in self.get_reachable(st):
            if self.accepted_states(dst):
                return None
            sinks.append(dst)
        return sinks

    def is_sink(self, st:S) -> bool:
        """
        Tests if a state is a sink node (only contains self loops)
        """
        return self._get_reachable_sinks(st) is not None


    def remove_sink_states(self) -> "NFA[S,A]":
        """
        Removes edges that directly connect to a sink node.
        """
        tsx = self.transition_func
        sink_cache:Dict[S,bool] = {}
        def is_sink(st:S) -> bool:
            snk = sink_cache.get(st, None)
            if snk is None:
                nodes = self._get_reachable_sinks(st)
                if nodes is None:
                    snk = sink_cache[st] = False
                else:
                    for n in nodes:
                        sink_cache[n] = True
                    snk = True
            return snk

        def transition_func(src:S, char:Optional[A]) -> AbstractSet[S]:
            if is_sink(src):
                return frozenset()
            return frozenset(filter(lambda x: not is_sink(x), tsx(src, char)))
        return NFA(self.alphabet, transition_func, self.start_state, self.accepted_states)


    def __len__(self) -> int:
        """
        Returns how many states the NFA contains
        """
        count = 0
        for st in self.states:
            count += 1
        return count

    def is_empty(self) -> bool:
        for _ in self.end_states:
            return False
        return True

    @property
    def end_states(self) -> Iterator[S]:
        return filter(self.accepted_states, self.states)

    def __repr__(self) -> str:
        data = dict(start_state=self.start_state,
                    accepted_states=list(self.end_states),
                    transitions=dict(self.as_graph()[1]))
        return f'NFA({data})'

    def as_dict(self:"NFA[S,A]", flatten:bool=True, state_serializer=nfa_state_serializer) -> Any:
        """
        Converts this NFA into a dictionary.
        """
        if flatten:
            nfa:NFA[Any,A] = self.flatten()
        else:
            nfa = self
        del self
        edges = list(
            dict(src=state_serializer(src), dst=state_serializer(dst), char=char)
            for ((src,dst),chars) in nfa.as_graph()[1].items()
                for char in chars
        )
        return dict(
            start_state = state_serializer(nfa.start_state),
            accepted_states = list(state_serializer(st) for st in nfa.end_states),
            edges = edges
        )

    @classmethod
    def from_dict(cls, nfa:Any, alphabet:Optional[Collection[A]]=None, state_deserializer=state_deserializer) -> "NFA[Any,A]":
        """
        Creates a NFA from a dictionary-based DFA (see `as_dict`).
        """
        if alphabet is None:
            alphabet = set(
                edge["char"]
                for edge in nfa["edges"]
                if edge["char"] is not None
            )

        tbl:Dict[Tuple[S,Optional[A]],Set[S]] = {}
        for edge in nfa["edges"]:
            src = state_deserializer(edge["src"])
            dst = state_deserializer(edge["dst"])
            src_char = (src, edge["char"])
            outs = tbl.get(src_char, None)
            if outs is None:
                tbl[src_char] = outs = set()
            outs.add(dst)

        return cls(alphabet=alphabet,
            transition_func=NFA.transition_table(tbl),
            start_state=state_deserializer(nfa["start_state"]),
            accepted_states=set(map(state_deserializer, nfa["accepted_states"])),
        )

    @classmethod
    def transition_edges_split(cls, edges:Iterable[Tuple[S,Optional[A],S]]) -> NTransitionFunc[S,A]:
        tsx:Dict[Tuple[S,Optional[A]],Set[S]] = {}
        for (src, char, dst) in edges:
            elems = tsx.get((src, char), None)
            if elems is None:
                elems = set()
                tsx[(src, char)] = elems
            elems.add(dst)
        return cls.transition_table(tsx)

    @classmethod
    def transition_edges(cls, edges:Iterable[Tuple[S,Iterable[Optional[A]],S]]) -> NTransitionFunc[S,A]:
        tsx:Dict[Tuple[S,Optional[A]],Set[S]] = {}
        for (src, chars, dst) in edges:
            for char in chars:
                elems = tsx.get((src, char), None)
                if elems is None:
                    elems = set()
                    tsx[(src, char)] = elems
                elems.add(dst)
        return cls.transition_table(tsx)

    @staticmethod
    def transition_table(table:Mapping[Tuple[S,Optional[A]],AbstractSet[S]]) -> NTransitionFunc[S,A]:
        def func(old_st:S, i:Optional[A]) -> AbstractSet[S]:
            return table.get((old_st, i), frozenset())
        return func

    @classmethod
    def from_transition_edges(cls, edges:Iterable[Tuple[S,Iterable[Optional[A]],S]], start_state:S, accepted_states:typing.Union[Collection[S],Callable[[S], bool]], alphabet:Optional[Collection[A]]=None) -> "NFA[S,A]":
        if alphabet is None:
            alphabet = set()
            for (_,chars,_) in edges:
                for c in chars:
                    if c is not None:
                        alphabet.add(c)
        return cls(frozenset(alphabet), cls.transition_edges(edges), start_state, accepted_states)

    @classmethod
    def from_transition_table(cls, table:TransitionTable[S,A], start_state:S, accepted_states:typing.Union[Collection[S],Callable[[S], bool]], alphabet:Optional[Collection[A]]=None) -> "NFA[S,A]":
        if alphabet is None:
            alphabet = set()
            for (_, c) in table:
                if c is not None:
                    alphabet.add(c)
            alphabet = frozenset(alphabet)
        return cls(alphabet, NFA.transition_table(table), start_state, accepted_states)

    # Usual NFA constructors

    @classmethod
    def make_nil(cls:"Type[NFA[int,A]]", alphabet:Collection[A]) -> "NFA[int, A]":
        Q1 = 0
        return cls(alphabet=alphabet,
                   transition_func=lambda q, a: frozenset(),
                   start_state=Q1,
                   accepted_states=lambda x: x == Q1)

    @classmethod
    def make_char(cls:"Type[NFA[int,A]]", alphabet:Collection[A], char:A) -> "NFA[int, A]":
        if char not in alphabet:
            raise ValueError(f"{repr(char)} not in {repr(alphabet)}")
        Q1, Q2 = range(2)
        dst_q2 = frozenset((Q2,))

        def transition(q:int, a:Optional[A]) -> AbstractSet[int]:
            return dst_q2 if a == char and q == Q1 else frozenset()

        return cls(alphabet, transition, Q1, [Q2])

    @classmethod
    def make_any_from(cls:"Type[NFA[int,A]]", chars:Collection[A], alphabet:Collection[A]) -> "NFA[int, A]":
        if not frozenset(chars) <= frozenset(alphabet):
            raise ValueError(f"{chars} not a subset of {alphabet}")
        Q1, Q2 = range(2)
        dst_q2 = frozenset((Q2,))

        def transition(q:int, a:Optional[A]) -> AbstractSet[int]:
            return dst_q2 if a in chars and q == Q1 else frozenset()

        return cls(alphabet, transition, Q1, [Q2])

    @classmethod
    def make_any(cls:"Type[NFA[int,A]]", alphabet:Collection[A]) -> "NFA[int, A]":
        Q1, Q2 = range(2)
        dst_q2 = frozenset((Q2,))

        def transition(q:int, a:Optional[A]) -> AbstractSet[int]:
            return dst_q2 if q == Q1 and a is not None else frozenset()

        return cls(alphabet, transition, Q1, [Q2])

    @classmethod
    def make_all(cls:"Type[NFA[int,A]]", alphabet:Collection[A]) -> "NFA[int, A]":
        Q1 = 0
        dst_q1 = frozenset((Q1,))

        def transition(q:int, a:Optional[A]) -> AbstractSet[int]:
            return dst_q1

        return cls(alphabet, transition, Q1, [Q1])

    @classmethod
    def make_contains(cls:"Type[NFA[int,A]]", alphabet:Collection[A], char:A) -> "NFA[int, A]":
        if char not in alphabet:
            raise ValueError(f"{repr(char)} not in {repr(alphabet)}")
        Q1, Q2 = range(2)
        dst_q2 = frozenset((Q2,))

        def transition(q:int, a:Optional[A]) -> AbstractSet[int]:
            if a == char and q == Q1:
                return dst_q2
            return frozenset((q,))

        return cls(alphabet, transition, Q1, [Q2])


    @classmethod
    def make_void(cls:"Type[NFA[int,A]]", alphabet:Collection[A]) -> "NFA[int,A]":
        Q1 = 0

        def transition(q:int, a:Optional[A]) -> AbstractSet[int]:
            return frozenset()

        return cls(alphabet, transition, Q1, lambda x: False)


def remove_back_edges(edges:Iterator[NEdge[S,A]], states:Collection[S]) -> Iterator[NEdge[S,A]]:
    return filter(lambda x: x[2] not in states, edges)

def edges_to_map(edges:Iterable[Tuple[S,Optional[A],S]]) -> TransitionTable[S,A]:
    result:Dict[Tuple[S,Optional[A]],Set[S]] = dict()
    for (src, char, dst) in edges:
        key = (src, char)
        if key in result:
            data = result[key]
        else:
            result[key] = data = set()
        data.add(dst)
    return result


@dataclass(frozen=True)
class Step(Generic[S,A]):
    source:Tuple[S,int]
    edge:Optional[A]
    target:Tuple[S,int]
    __iter__ = astuple
    def __lt__(self, other):
        return astuple(self) < astuple(other)

NDerivation = Tuple[List[List[Step[S,A]]], List[Tuple[S,int]] ]

def reduction_tree(levels:List[FrozenSet[NEdge[S,A]]], accepted_states:Collection[S]) -> Tuple[List[List[Step[S,A]]], List[Tuple[S,int]] ]:
    result:List[List[Step[S,A]]] = []
    nodes:Dict[S,int] = {}
    for lvl in levels:
        lvl_map = edges_to_map(lvl)
        edges:List[Step[S,A]] = []
        next_nodes:Dict[S,int] = dict(nodes)
        for (_, outs) in lvl_map.items():
            for st in outs:
                next_nodes[st] = nodes.get(st, 0) + 1

        for ((src_st, char), outs) in sorted(lvl_map.items()):
            for dst_st in outs:
                prev_nodes = nodes if char is not None else next_nodes
                edges.append(
                    Step(source=(src_st, prev_nodes.get(src_st, 0)),
                        edge=char,
                        target=(dst_st, next_nodes.get(dst_st, 0))
                    )
                )
        nodes = next_nodes
        result.append(edges)

    accepted = list((st, nodes.get(st, 0)) for st in accepted_states)
    accepted.sort()
    return result, accepted

class DerivationGraph(Generic[S,A]):
    def __init__(self, alphabet:Collection[A], start_state:S, transition_func:NTransitionFunc[S,A], accepted_states:Callable[[S], bool]) -> None:
        self.alphabet = alphabet
        self.start_state = start_state
        self.transition_func = transition_func
        self.accepted_states = accepted_states

    def multi_transition(self, states:Iterable[S], char:Optional[A]) -> AbstractSet[S]:
        return multi_transition(self.transition_func, states, char)

    def epsilon(self, states:Iterable[S]) -> AbstractSet[S]:
        return epsilon(self.transition_func, states)


    def multi_transition_edges(self, states:Iterable[S], char:Optional[A]) -> Iterator[Tuple[S,Optional[A],S]]:
        """
        Returns the outgoing edges from a set of states.
        """
        for src in states:
            for dst in self.transition_func(src, char):
                yield (src, char, dst)

    def _epsilon_edges_unfold_hops(self, states:Collection[S]) -> Iterator[FrozenSet[NEdge[S,A]]]:
        row:FrozenSet[NEdge[S,A]] = frozenset(remove_back_edges(self.multi_transition_edges(states, None), states))
        all_edges = set(row)
        curr_states = frozenset(states)
        next_states = curr_states | self.multi_transition(curr_states, None)
        while len(next_states) != len(curr_states):
            yield row
            curr_states = next_states
            mut_row = set()
            for edge in self.multi_transition_edges(curr_states, None):
                # Make sure that the edge is new and is not a back edge
                if edge not in all_edges and edge[2] not in curr_states:
                    all_edges.add(edge)
                    mut_row.add(edge)
            row = frozenset(mut_row)
            next_states = curr_states | self.multi_transition(curr_states, None)

    def _epsilon_edges_fold_hops(self, states:Collection[S]) -> Iterator[FrozenSet[NEdge[S,A]]]:
        result:Set[NEdge[S,A]] = set()
        for row in self._epsilon_edges_unfold_hops(states):
            result.update(row)
        if len(result) > 0:
            yield frozenset(result)

    def epsilon_edges(self, states:Collection[S], fold_hops:bool=False) -> Iterator[FrozenSet[NEdge[S,A]]]:
        if fold_hops:
            return self._epsilon_edges_fold_hops(states)
        else:
            return self._epsilon_edges_unfold_hops(states)

    def accepts_edges(self, inputs:Iterable[A], fold_epsilon_hops:bool=False) -> Tuple[List[FrozenSet[NEdge[S,A]]],Set[S]]:
        states = self.epsilon({self.start_state})
        frontier:List[FrozenSet[NEdge[S,A]]] = []
        for lvl in self.epsilon_edges({self.start_state}, fold_hops=fold_epsilon_hops):
            frontier.append(frozenset(lvl))

        for i in inputs:
            if len(states) == 0:
                return frontier, set()
            frontier.append(frozenset(self.multi_transition_edges(states, i)))
            states = self.multi_transition(states, i)
            for lvl in self.epsilon_edges(states, fold_hops=fold_epsilon_hops):
                frontier.append(frozenset(lvl))
            states = epsilon(self.transition_func, states)

        return frontier, set(st for st in states if self.accepted_states(st))

    def accepts_derivation(self, word:Iterable[A], fold_epsilon_hops:bool=False) -> NDerivation[S,A]:
        lvls, final = self.accepts_edges(word, fold_epsilon_hops=fold_epsilon_hops)
        return reduction_tree(lvls, final)

def make_nfa_derivation_graph(nfa:NFA[S,A]) -> DerivationGraph[S,A]:
    return DerivationGraph(
        alphabet=nfa.alphabet,
        start_state=nfa.start_state,
        transition_func=nfa.transition_func,
        accepted_states=nfa.accepted_states
    )


def nfa_derivation_graph(nfa:NFA[S,A], word:Collection[A]) -> NDerivation[S,A]:
    der = make_nfa_derivation_graph(nfa)
    return der.accepts_derivation(word)

def nfa_to_dfa(nfa:NFA[S,A]) -> DFA[AbstractSet[S],A]:
    def transition(q:AbstractSet[S], c:A) -> AbstractSet[S]:
        if not isinstance(q, frozenset):
            raise ValueError(q)
        return nfa.epsilon(nfa.multi_transition(q, c))

    def accept_state(qs:AbstractSet[S]) -> bool:
        for q in qs:
            if nfa.accepted_states(q):
                return True
        return False

    return DFA(alphabet=nfa.alphabet,
                transition_func=transition,
                start_state=nfa.epsilon({nfa.start_state}),
                accepted_states=accept_state)


def dfa_to_nfa(dfa:DFA[S,A]) -> NFA[S,A]:
    def transition_func(q:S, a:Optional[A]) -> AbstractSet[S]:
        return {
                dfa.transition_func(q, a),
            } if a is not None else frozenset()

    return NFA(alphabet=dfa.alphabet,
                transition_func=transition_func,
                start_state=dfa.start_state,
                accepted_states=dfa.accepted_states)

################################################################################
# REGEX

class RegexHandler(Generic[A,S]):
    on_nil : S
    on_void : S

    def on_concat(self, reg:"Concat[A]", left:S, right:S) -> S:
        raise NotImplementedError()

    def on_union(self, reg:"Union[A]", left:S, right:S) -> S:
        raise NotImplementedError()

    def on_star(self, reg:"Star[A]", child:S) -> S:
        raise NotImplementedError()

    def on_char(self, reg:"Char[A]") -> S:
        raise NotImplementedError()

class RegexToStr(RegexHandler[A,str]):
    def __init__(self,
                char_str:Callable[[A],str],
                void_str:str,
                nil_str:str,
                app_str:Callable[[str,str],str],
                union_str:Callable[[str,str],str],
                star_str:Callable[[str],str]):
        self.char_str = char_str
        self.on_void = void_str
        self.on_nil = nil_str
        self.app_str = app_str
        self.union_str = union_str
        self.star_str = star_str

    def paren(self, r:"Regex[A]", text:str) -> str:
        return text if is_primitive(r) else f"({text})"

    def on_concat(self, r:"Concat[A]", left:str, right:str) -> str:
        return self.app_str(self.paren(r.left, left), self.paren(r.right, right))

    def on_union(self, r:"Union[A]", left:str, right:str) -> str:
        p_left = self.paren(r.left, left)
        p_right = self.paren(r.right, right)
        if isinstance(r.left, Union) and isinstance(r.right, Union):
            return self.union_str(left, right)

        elif isinstance(r.left, Union):
            return self.union_str(left, p_right)

        elif isinstance(r.right, Union):
            return self.union_str(p_left, right)

        return self.union_str(p_left, p_right)

    def on_star(self, r:"Star[A]", child:str) -> str:
        return self.star_str(self.paren(r.child, child))

    def on_char(self, r:"Char[A]") -> str:
        return self.char_str(r.char)

class Serialize(RegexHandler[A,Any]):
    def __init__(self, serialize_char:Callable[[A],Any]=lambda x:x):
        self.serialize_char = serialize_char

    on_nil:Any = tuple()
    on_void = None

    def on_concat(self, reg:"Concat[A]", left:"Regex[A]", right:"Regex[A]") -> Any:
        return dict(concat=dict(left=left, right=right))

    def on_union(self, reg:"Union[A]", left:"Regex[A]", right:"Regex[A]") -> Any:
        return dict(union=dict(left=left, right=right))

    def on_star(self, reg:"Star[A]", child:"Regex[A]") -> Any:
        return dict(star=child)

    def on_char(self, reg:"Char[A]") -> Any:
        return dict(char=self.serialize_char(reg.char))


class Regex(Generic[A]):

    def fold(reg, handler:RegexHandler[A,S]) -> S:
        if isinstance(reg, Union):
            left = reg.left.fold(handler)
            right = reg.right.fold(handler)
            return handler.on_union(reg, left, right)
        elif isinstance(reg, Concat):
            left = reg.left.fold(handler)
            right = reg.right.fold(handler)
            return handler.on_concat(reg, left, right)
        elif isinstance(reg, Star):
            child = reg.child.fold(handler)
            return handler.on_star(reg, child)
        elif reg == VOID:
            return handler.on_void
        elif reg == NIL:
            return handler.on_nil
        elif isinstance(reg, Char):
            return handler.on_char(reg)
        raise ValueError(reg)

    def exists(reg, pred:"Callable[[Regex[A]],bool]") -> bool:
        if isinstance(reg, Union):
            return reg.left.exists(pred) or \
                    reg.right.exists(pred) or \
                    pred(reg)
        elif isinstance(reg, Concat):
            return reg.left.exists(pred) or \
                    reg.right.exists(pred) or \
                    pred(reg)
        elif isinstance(reg, Star):
            return reg.child.exists(pred) or pred(reg)
        elif reg == VOID:
            return pred(reg)
        elif reg == NIL:
            return pred(reg)
        raise ValueError(reg)

    def to_string(reg:"Regex[A]", char_str:Callable[[A],str]=str, void_str:str="{}", nil_str:str="[]",
            app_str:Callable[[str,str],str]=lambda x, y:x + " \\cdot " + y,
            union_str:Callable[[str,str],str]=lambda x, y: x + " + " + y,
            star_str:Callable[[str],str]=lambda x: x + "*") -> str:
        return reg.fold(RegexToStr(char_str, void_str, nil_str, app_str, union_str, star_str))

    def serialize(self, serialize_char:Callable[[A],Any]=lambda x:x) -> Any:
        return self.fold(Serialize(serialize_char))

    @classmethod
    def from_dict(cls:"Type[Regex[A]]", node:Any, parse_char:Callable[[Any],A]=lambda x:cast(A,x)) -> "Regex[A]":
        rec:Callable[[Any],Regex[A]] = lambda x: cls.from_dict(x, parse_char=parse_char)
        if node == []:
            return cast(Regex[A], NIL)
        elif node is None:
            return cast(Regex[A], VOID)
        if not (isinstance(node, dict) and len(node) == 1):
            raise ValueError("Expecting a dictionary with one key, but got:", node)
        # There is only one key value, unpack it
        (key, data), = node.items()
        if key == "star":
            child = rec(data)
            return Star(child)
        elif key == "union":
            left = rec(data["left"])
            right = rec(data["right"])
            return Union(left, right)
        elif key == "app":
            left = rec(data["left"])
            right = rec(data["right"])
            return Concat(left, right)
        elif key == "char":
            return Char(parse_char(data))
        raise ValueError(f"Unknown key={repr(key)}, data={data}")

@dataclass(frozen=True)
class Void(Regex[A]):
    pass

VOID:Regex[Any] = Void()

@dataclass(frozen=True)
class Nil(Regex[A]):
    pass

NIL:Regex[Any] = Nil()

@dataclass(frozen=True)
class Char(Regex[A]):
    char: A

def is_primitive(r:Regex[A]) -> bool:
    return r == VOID or r == NIL or isinstance(r, Char)


@dataclass(frozen=True)
class Concat(Regex[A]):
    left: Regex[A]
    right: Regex[A]

    __iter__ = astuple


    @staticmethod
    def from_list(args:Iterable[Regex[A]]) -> Regex[A]:
        result = NIL
        for elem in args:
            result = concat(result, elem)
        return result


def concat(left:Regex[A], right:Regex[A]) -> Regex[A]:
    if left == NIL:
        return right
    if right == NIL:
        return left
    if right == VOID or left == VOID:
        return VOID
    return Concat(left, right)

@dataclass(frozen=True)
class Union(Regex[A]):
    left:Regex[A]
    right:Regex[A]

    __iter__ = astuple

    def flatten(n) -> Set[Regex[A]]:
        result:Set[Regex[A]] = set()
        to_visit:List[Regex[A]] = [n]
        while len(to_visit) > 0:
            r = to_visit.pop()
            if isinstance(r, Union):
                to_visit.extend(astuple(r))
            elif r is not VOID:
                result.add(r)
        return result

    @classmethod
    def from_list(cls, iterable:Iterator[Regex[A]]) -> Regex[A]:
        result:Regex[A] = VOID
        for x in iterable:
            if isinstance(result, Void):
                result = x
            else:
                result = cls(x, result)
        return result



def union(left:Regex[A], right:Regex[A]) -> Regex[A]:
    if left == VOID:
        return right
    if right == VOID:
        return left
    if isinstance(right, Star):
        if isinstance(left, Nil):
            return right
    if isinstance(left, Star):
        if isinstance(right, Nil):
            return left
    return Union(left, right)



def shuffle(c1:Regex[A], c2:Regex[A]) -> Regex[A]:
    return Union(concat(c1, c2), concat(c2, c1))


@dataclass(frozen=True)
class Star(Regex[A]):
    child: Regex[A]
    __iter__ = astuple


def star(child:Regex[A]) -> Regex[A]:
    if child == NIL:
        return NIL
    if child == VOID:
        return NIL
    if isinstance(child, Star):
        return child
    return Star(child)


class SubstHandler(RegexHandler[A,Regex[A]]):
    """
    A regular expression handler that returns a regular expression.
    It is smart in that it does not re-create parts of the tree that remain
    unchanged.
    """
    on_nil = NIL
    on_void = VOID
    def on_char(self, char:Regex[A]) -> Regex[A]:
        return char
    def on_concat(self, reg:Concat[A], left:Regex[A], right:Regex[A]) -> Regex[A]:
        return reg if reg.left is left and reg.right is right else Concat(left, right)
    def on_union(self, reg:Union[A], left:Regex[A], right:Regex[A]) -> Regex[A]:
        return reg if reg.left is left and reg.right is right else Union(left, right)
    def on_star(self, reg:Star[A], child:Regex[A]) -> Regex[A]:
        return reg if reg.child is child else Star(child)


def get_alphabet(r:Regex[A]) -> Set[A]:
    to_process:List[Regex[A]] = [r]
    result:Set[A] = set()
    while len(to_process) > 0:
        elem = to_process.pop()
        if isinstance(elem, Char):
            result.add(elem.char)
        elif isinstance(elem, Void) or isinstance(elem, Nil):
            pass
        elif isinstance(elem, Star):
            to_process.append(elem.child)
        else:
            assert isinstance(elem, Union) or isinstance(elem, Concat), str(elem)
            to_process.append(elem.left)
            to_process.append(elem.right)
    return result

class WrapState(Generic[S]):
    def _to_tuple(self) -> Tuple[int, Optional[S]]:
        raise NotImplementedError()

    def __lt__(self, other:Any) -> bool:
        if not isinstance(other, WrapState):
            raise ValueError()
        return self._to_tuple() < other._to_tuple()

@dataclass(frozen=True)
class WrapState_Start(WrapState[S]):
    def _to_tuple(self) -> Tuple[int, None]:
        return (0, None)

@dataclass(frozen=True)
class WrapState_Mid(WrapState[S]):
    state:S
    def _to_tuple(self) -> Tuple[int, S]:
        return (1, self.state)

@dataclass(frozen=True)
class WrapState_End(WrapState[S]):
    def _to_tuple(self) -> Tuple[int, None]:
        return (2, None)

def gnfa_state_serializer(st):
    if isinstance(st, WrapState_Start):
        return 0
    if isinstance(st, WrapState_End):
        return 2
    if isinstance(st, WrapState_Mid):
        return [1, st.state]

def wrap(nfa:NFA[S,A]) -> NFA[WrapState[S], A]:
    start:WrapState[S] = WrapState_Start()
    end:WrapState[S] = WrapState_End()
    init = WrapState_Mid(nfa.start_state)
    init_s:AbstractSet[WrapState[S]] = frozenset([init])
    def tsx(st:WrapState[S], a:Optional[A]) -> AbstractSet[WrapState[S]]:
        if isinstance(st, WrapState_Start):
            return init_s if a is None else frozenset()
        elif isinstance(st, WrapState_End):
            return frozenset()
        elif isinstance(st, WrapState_Mid):
            rest:FrozenSet[WrapState[S]] = frozenset(map(WrapState_Mid, nfa.transition_func(st.state, a)))
            if nfa.accepted_states(st.state) and a is None:
                return rest.union({end})
            return rest
        raise ValueError(st)

    return NFA(alphabet=nfa.alphabet,
               transition_func=tsx,
               start_state=start,
               accepted_states=[end])


class GNFA(Generic[S,A]):
    def __init__(self, mid_states:List[S], alphabet:Collection[A], transitions:Mapping[Tuple[S,S],Regex[A]], start_state:S, end_state:S):
        self.mid_states = mid_states
        self.alphabet = alphabet
        self.transitions = transitions
        self.start_state = start_state
        self.end_state = end_state

    def get_state_index(self, st):
        for idx, m in enumerate(self.mid_states):
            if m == st:
                return idx
        return None

    @property
    def states(self):
        return [self.start_state] + self.mid_states + [self.end_state]

    def as_graph(self) -> StateDiagram[S,Regex[A]]:
        return self.states, dict(
            (x, (y,)) for x, y in self.transitions.items())

    def accepted_states(self, st:S) -> bool:
        return st == self.end_state

    def next_node(self, index=0):
        return self.mid_states[index]

    def can_reduce(self):
        return len(self.mid_states) > 0

    def next_edges(self, node):
        states = self.states
        edges = []
        after = []
        node_node = (node, node) in self.transitions
        if node_node:
            edges.append((node, node))
        for qi in states:
            if qi == node:
                continue
            for qj in states:
                if qj == node:
                    continue
                r1_qi_node = (qi, node) in self.transitions
                r3_node_qj = (node, qj) in self.transitions
                r4_qi_qj = (qi, qj) in self.transitions
                if r1_qi_node and r3_node_qj:
                    edges.append((qi, node))
                    edges.append((node, qj))
                    if r4_qi_qj:
                        edges.append((qi, qj))
                    after.append((qi, qj))
        return edges, after

    def step(self, index:Optional[int]=None) -> "GNFA[S,A]":
        if len(self.mid_states) == 0:
            raise ValueError("Cannot reduce any further.")
        # Pick a state that is not start/end
        if index is None:
            index = 0
        if index == 0:
            mid_states = self.mid_states[1:]
        else:
            mid_states = self.mid_states[0:index] + self.mid_states[index + 1:]
        states = list(mid_states)
        states.append(self.start_state)
        states.append(self.end_state)
        tsx = {}
        node = self.mid_states[index]
        r2 = self.transitions.get((node, node), VOID)
        for qi in states:
            for qj in states:
                r1 = self.transitions.get((qi, node), VOID)
                r3 = self.transitions.get((node, qj), VOID)
                r4 = self.transitions.get((qi, qj), VOID)
                result = union(concat(r1, concat(star(r2), r3)), r4)
                if result is not VOID:
                    tsx[(qi, qj)] = result
        gnfa = GNFA(mid_states, self.alphabet, tsx, self.start_state,
                    self.end_state)
        return gnfa

    def reduce(self) -> "Iterator[GNFA[S,A]]":
        gnfa = self
        while len(gnfa.states) > 2:
            gnfa = gnfa.step()
            yield gnfa

    def as_dict(self, state_serializer=gnfa_state_serializer) -> Any:
        """
        Converts this GNFA into a dictionary.
        """
        edges = [
            dict(src=state_serializer(src), dst=state_serializer(dst), char=char)
            for ((src,dst), char) in self.transitions.items()
        ]
        return dict(
            start_state = state_serializer(self.start_state),
            accepted_states = [state_serializer(self.end_state)],
            edges = edges
        )

    def to_regex(gnfa) -> Regex[A]:
        m = list(gnfa.reduce())
        return m[-1].transitions.get((gnfa.start_state, gnfa.end_state), NIL)

    @classmethod
    def from_nfa(cls:"Type[GNFA[Any,A]]", nfa:NFA[S,A]) -> "GNFA[Any,A]":
        wrapped_nfa = wrap(nfa)
        start = wrapped_nfa.start_state
        end, = wrapped_nfa.end_states
        mid_states = [st for st in wrapped_nfa.states if st != start and st != end]
        _, edges = wrapped_nfa.as_graph()
        tsx = {}
        for (q1, q2), chars in edges.items():
            all_chars:Iterator[Regex[A]] = (Char(a) if a is not None else NIL for a in chars)
            tsx[(q1, q2)] = Union.from_list(all_chars)

        return cls(
            mid_states=mid_states,
            alphabet=wrapped_nfa.alphabet,
            transitions=tsx,
            start_state=start,
            end_state=end,
        )

def regex_to_nfa(r:Regex[A], alphabet:Optional[Collection[A]]=None) -> NFA[Any,A]:
    if alphabet is None:
        # Compute the alphabet automatically
        alphabet = get_alphabet(r)
    if not isinstance(r, Regex):
        raise ValueError(r, type(r))
    if r == VOID:
        return NFA[int,A].make_void(alphabet)
    elif r == NIL:
        return NFA[int,A].make_nil(alphabet)
    elif isinstance(r, Char):
        return NFA[int,A].make_char(alphabet, r.char)
    elif isinstance(r, Concat):
        left = regex_to_nfa(r.left, alphabet)
        right = regex_to_nfa(r.right, alphabet)
        return left.concat(right)
    elif isinstance(r, Union):
        left = regex_to_nfa(r.left, alphabet)
        right = regex_to_nfa(r.right, alphabet)
        return left.union(right)
    elif isinstance(r, Star):
        child = regex_to_nfa(r.child, alphabet)
        return child.star()
    raise ValueError(r)


def nfa_to_regex(nfa: NFA[S,A]) -> Regex[A]:
    # If there are no accepting states, then its language is the empty set.
    if nfa.is_empty():
        return VOID
    g = GNFA.from_nfa(nfa)
    return g.to_regex()
