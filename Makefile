PIP=pip3
all: test check
PYTEST_FLAGS ?=

test: check
	PYTHONPATH=. pytest tests $(PYTEST_FLAGS)
check:
	 mypy karakuri
install:
	$(PIP) install .

.PHONY: all test install
