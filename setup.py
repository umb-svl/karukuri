import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="karakuri",
    version="0.0.1",
    author="Tiago Cogumbreiro",
    author_email="cogumbreiro@users.sf.net",
    description="Karakuri includes algorithms related to regular languages: finite-state machines (FSM), deterministic automata (DFA), non-deterministic automata (NFA), regular expressions (REGEX).",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/cogumbreiro/karuki",
    packages=setuptools.find_packages(),
    package_data= {'karakuri': ['py.typed']},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.0',
)
